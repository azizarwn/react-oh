import React, { useReducer, useEffect } from 'react';
import axios from 'axios';
import Navbarx from "../component/navbar";
import Footer from "../component/footer";
import "./user.scss";

//declare initialState = state variable
const initialState = {
	loading: true, //loading indicator
	error: '',	//error var
	post: {}	//for hold data berupa objek 
}

const reducer = (state, action) => {
	switch (action.type) {
		case 'FETCH_SUCCESS':
			return {
				loading: false,
				post: action.payload,
				error: ''
			}
		case 'FETCH_ERROR':
			return {
				loading: false,
				post: [], //ini berupa array kosong
				error: 'Something went wrong!'
			}
		default:
			return state
	}
}

function DataFetching() {
	const [state, dispatch] = useReducer(reducer, initialState)

	useEffect(() => {
		axios
			.get(`https://jsonplaceholder.typicode.com/users`)
			.then(response => {
				console.log(response)
				dispatch({ type: 'FETCH_SUCCESS', payload: response.data })
			})
			.catch(error => {
				dispatch({ type: 'FETCH_ERROR' })
			})
	}, [])
	return (
		<>
		<Navbarx />
		<div className='user-data'>
			<h2>Fetching data with useReducer </h2>
			{state.loading ? 'Loading' : state.post.map((user, index) => (
                <div className="user-item" key={index}>
                <p>{index + 1}. Name: {user.name}</p>
				<p>User Name: {user.username}</p>
                <p>Email: {user.email}</p>
				<p>Address:	{user.address.street}, 
					{user.address.suite} 
					{user.address.city}, 
					{user.address.zipcode}.
				</p>
                </div>
            ))}
			{state.error ? state.error : null}
		</div>
		<Footer />
		</>
	)
}

export default DataFetching;