import React, { useEffect, useState } from "react";
import TodoForm from "../component/todo/todoForm";
import TodoList from "../component/todo/todoList";
import Navbarx from "../component/navbar"
import Footer from "../component/footer"
import "./Mytodo.css"

const LOCAL_STORAGE_KEY = "todo-list-todos";

function Mytodo() {
    const [todos, setTodos] = useState([]);

    useEffect(() => {
        const storageTodos = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY))
        if (storageTodos) {
            setTodos(storageTodos);
        }
    }, []);    

    useEffect(() => {
        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(todos));
    }, [todos]);
    
    function addTodo(todo) {
        setTodos([todo, ...todos]);
    }

    function toggleComplete(id) {
        setTodos(
            todos.map(todo => {
                if (todo.id === id) {
                    return {
                        ...todo,
                        completed: !todo.completed
                    };
                }
                return todo;
            })
        );
    }

    function removeTodo(id) {
        setTodos(todos.filter(todo => todo.id !== id));
    }

    return (
        <div className="todo">
            <Navbarx/>
            <header className="todo-header">
                <h1>To Do List</h1>
                <TodoForm addTodo={addTodo} />
                <br/>
                <TodoList 
                    todos={todos} 
                    toggleComplete={toggleComplete}
                    removeTodo={removeTodo}
                />
            </header>
            <Footer />
        </div>
    )
}

export default Mytodo;
