import React, { useState } from "react";
import {FormInput} from "../component/todo/formSimple";
import "./todoSimple.css";
import Navbarx from "../component/navbar";
import Footer from "../component/footer";
import { Button } from 'react-bootstrap';



export const SimpleTodo = () => {
  const [todos, setTodos] = useState([]);

  const toggleComplete = i =>
    setTodos(
      todos.map(
        (todo, k) =>
          k === i
            ? {
                ...todo,
                complete: !todo.complete
              }
            : todo
      )
    );

  return (
    <>
    <Navbarx />
    <div className="todo-simple">
      <FormInput
        onSubmit={text => setTodos([{ text, complete: false }, ...todos])}
      />
      <div>
        {todos.map(({ text, complete }, i) => (
          <div
            key={text}
            onClick={() => toggleComplete(i)}
            style={{
                color:"black",
                fontFamily: "Arial",
                fontSize: "1rem",
                fontWeight: "700",
                margin: "1rem 0 1rem 4rem",
                textAlign: "center",
                textDecoration: complete ? "line-through" : ""
            }}
          >
            {text}
          </div>
        ))}
        
      </div>
      <Button 
        type="submit" 
        variant="outline-light"
        style={{
            marginLeft: "4rem",
            backgroundColor: "#F18C8E"
        }} 
        onClick={() => setTodos([])}>
            Reset Task</Button>
    </div>
    <Footer />
    </>
  );
};