import React, { createContext, useContext, useState } from 'react';
import { Button } from 'react-bootstrap';
import Navbarx from "../component/navbar";
import SideLeft from "../component/sideLeft";
import Footer from "../component/footer";
import "./profileCtxt.css";
import pic from "./pic.jpg";


function ProfileCtxt () {
    const themes = {
        light : {
            background : '#f0f0f0',
            color : '#ff4079',
            fontFamily: 'calibri'
        },
        dark : {
            background : '#2e2e2e',
            color : '#F0B7A4',
            fontFamily: 'segoe'
        }
    };

    const [ dark, setDark] = useState(false);
    const changeTheme = () => {
        setDark(!dark);
    };

    const ThemeContext = createContext(themes.light);

    const ButtonChange = () => {
        const theme = useContext(ThemeContext);

        return ( 
            <div>
                <div
                style={{
                    background: theme.background,
                    color: theme.color,
                    fontFamily: theme.fontFamily
                }}
                >
                <Navbarx />
                <div className="main-content">
                    <SideLeft />
                    <div className="context">
                    <Button className="btnCtxt" variant="outline-dark" onClick={changeTheme}>Change Themes</Button>
                    <div className='profile-container'>
                    <img src={pic} className="pic" alt="profil"/>
                    <div className="pic-text">
                        <h2>About Me</h2>
                        <hr/>
                        <p> I was born on July, 24th 1998 in Pati, Central Java. 
                        In September 2020, I graduated from State Polytechnic of Batam. I'm a Glints Academy 
                        Student based in Batam City. Currently, I'm learning about HTML, CSS, Bootstrap, Javascript, React, and GIT.
                        My goal is to be a Professional Front End Developer.
                        </p>
                        <p> I enjoy travelling to a new place and when I'm just staying at home I'll be 
                        marathons on my Netflix or cuddling my cats. Feel free to get in touch with me.
                        </p>
                    </div>
                    </div>
                    <br/>
                    {/* <Footer />  */}
                    <div>
                    </div>
                    </div>
                </div>
                <Footer />  
                </div>

            </div>
        );
    };

    return (
        <ThemeContext.Provider value={dark? themes.dark : themes.light}>
            
            <ButtonChange />
 
        </ThemeContext.Provider>
    )

}

export default ProfileCtxt;