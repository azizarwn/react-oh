import React, { useEffect, useReducer } from "react";
import axios from 'axios';
import Navbarx from "../component/navbar"
import Footer from "../component/footer"
import PostCard from "../component/postCard";

function PostData() {

    const initialState = {
        loading: true, //loading indicator
        error: '',	//error var
        post: {}	//for hold data berupa objek 
    }

    const reducer = (state, action) => {
        switch (action.type) {
            case 'FETCH_SUCCESS':
                return {
                    loading: false,
                    post: action.payload,
                    error: ''
                }
            case 'FETCH_ERROR':
                return {
                    loading: false,
                    post: [], //ini berupa array kosong
                    error: 'Something went wrong!'
                }
            default:
                return state
        }
    }

    const [state, dispatch] = useReducer(reducer, initialState);

    const reducerGet = () => {
        axios
        .get(`https://jsonplaceholder.typicode.com/posts`)
			.then(response => {
				console.log(response)
				dispatch({ type: 'FETCH_SUCCESS', payload: response.data })
			})
			.catch(error => {
				dispatch({ type: 'FETCH_ERROR' })
			})
    }

    useEffect(() => {
        reducerGet()
	}, [])

    return (
        <>
        <Navbarx />
        <PostCard state={state}/>
        <Footer />
        </>
        )
}

export default PostData; 