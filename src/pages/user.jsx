import React, { useState, useEffect } from "react";
import axios from 'axios';
import Navbarx from "../component/navbar"
import Footer from "../component/footer"
import "./user.scss";


function UserData() {
    const [users, setUsers] = useState([])
    
    //fetching data
    useEffect(() => {
        axios
            .get('https://jsonplaceholder.typicode.com/users')
            .then(response => {
                console.log(response)
                setUsers(response.data)
            })
            .catch(error => {
                console.log(error)
            })
    })

    return (
    <>
    <Navbarx />
        <div className='user-data'>
        <h2>Fetching data with useState</h2>
            {users.map(user => (
                <div className="user-item" key={user.id}>
                <p>{user.id}. Name: {user.name}</p>
                <p>User Name: {user.username}</p>
                <p>Email: {user.email}</p>
                <p>Address: {user.address.street}, {user.address.suite} {user.address.city}, {user.address.zipcode}.</p>
                </div>
            ))}
        
        </div>
    <Footer />
    </>
    )
}

export default UserData; 