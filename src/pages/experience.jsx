import React from "react"; //ini gak wajib, cuma klo pakai properti dari react, wajib pakai.
import "./experience.css";
import Navbarx from "../component/navbar"
import Footer from "../component/footer"
import { Card, Button } from 'react-bootstrap';
import card1 from "./card1.jpg";
import card2 from "./card2.jpg";
import card3 from "./card3.jpg";

function Exper() {
    return (
        <>
        <div className="work">
        <Navbarx/>
        <div className="card-item">
        <div>
        <Card className="card-exper" style={{ width: '18rem' }}>
            <Card.Img variant="top" src={card1} />
            <Card.Body>
                <Card.Title>Photography</Card.Title>
                <Card.Text>
                My Last Photography Project was the Borobudur Temple picture taken in 2018.
                </Card.Text>
                <br/>
            <Button variant="primary" href="https://drive.google.com/file/d/1ozG4KG-EH-U1j7sv22mQGC4voSU_kR9-/view?usp=sharing">See Project</Button>
            </Card.Body>
        </Card>
        </div>
        <div>
        <Card className="card-exper" style={{ width: '18rem' }}>
            <Card.Img variant="top" src={card2} />
            <Card.Body>
                <Card.Title>Videography</Card.Title>
                <Card.Text>
                One of My Videography projects is Dekade short movie. 
                It nominated for the 3rd best fiction short movie in Polibatam film festival 2018. 
                </Card.Text>
            <Button variant="primary" href="https://drive.google.com/file/d/1X4s3zOFfbZfcmDma5D3PVu6P_VgHk6tr/view?usp=sharing">See Project</Button>
            </Card.Body>
        </Card>
        </div>
        <div>
        <Card className="card-exper" style={{ width: '18rem' }}>
            <Card.Img variant="top" src={card3} />
            <Card.Body>
                <Card.Title>Animation</Card.Title>
                <Card.Text>
                It's my animation film project featuring Polibatam Team about 
                Posyandu socialization for pregnant women. 
                </Card.Text>
            <Button variant="primary" href="https://drive.google.com/file/d/1cwrsGG6vCxdHOs4N2F4g9lVQBsbSpTcc/view?usp=sharing">See Project</Button>
            </Card.Body>
        </Card>
        </div>
        </div>

        <br/><br/>
        <Footer />
        </div>
        
        </>
    )
} 

export default Exper;