import React from "react"; //ini gak wajib, cuma klo pakai properti dari react, wajib pakai.
import "./contact.css";
import Navbarx from "../component/navbar"
import Footer from "../component/footer"
import { Form, Button } from 'react-bootstrap';

function MyContact() {
    return (
        <>
        <div className="contact">
        <Navbarx/>
        <Form >
            <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="msgranger@mail.com" />
                <Form.Text style={{color: "white"}}>
                We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>
            <br/>
            <Form.Group controlId="formBasicType">
                <Form.Label>First Name</Form.Label>
                <Form.Control type="type" placeholder="Hermione" />
                <Form.Label>Last Name</Form.Label>
                <Form.Control type="type" placeholder="Granger" />
            </Form.Group>
            <br/>
            <Form.Group controlId="exampleForm.ControlTextarea1">
                <Form.Label>Message</Form.Label>
                <Form.Control as="textarea" rows={3} placeholder="Dear Aziza,"/>
            </Form.Group>

            <Button type="submit" variant="outline-light">
                Send Message
            </Button>
        </Form>
        <br/><br/>
        <Footer />
        </div>
        
        </>
    )
} 

export default MyContact;