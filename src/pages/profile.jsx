import React from "react";
import Navbarx from "../component/navbar"
import SideLeft from "../component/sideLeft"
import Footer from "../component/footer"
import "./profile.css";
import pic from "./pic.jpg";


function Profile() {
    return (
      <>
      <Navbarx />
       <div className="main-content">
        <SideLeft />
        <div>
        <div className='profile-container'>
          <img src={pic} className="pic" alt="profil"/>
          <div className="pic-text">
            <h2>About Me</h2>
            <hr/>
            <p> I was born on July, 24th 1998 in Pati, Central Java. 
            In September 2020, I graduated from State Polytechnic of Batam. I'm a Glints Academy 
            Student based in Batam City. Currently, I'm learning about HTML, CSS, Bootstrap, Javascript, React, and GIT.
            My goal is to be a Professional Front End Developer.
            </p>
            <p> I enjoy travelling to a new place and when I'm just staying at home I'll be 
            marathons on my Netflix or cuddling my cats. Feel free to get in touch with me.
            </p>
          </div>
        </div>
        <br/>
        <Footer /> 
        <div>
        
        </div>
        </div>
      
      </div> 
      
      </>
    )
}

export default Profile;