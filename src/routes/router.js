import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Landing from "../pages/Landing";
//import Profile from "../pages/profile";
import MyContact from "../pages/contact";
import Exper from "../pages/experience";
import Mytodo from "../pages/Mytodo";
import {SimpleTodo} from "../pages/todoSimple";
import UserData from "../pages/user";
import DataFetching from "../pages/userProfile";
import PostData from "../pages/post";
import ProfileCtxt from "../pages/profileCtxt";



const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact>
          <Landing />
        </Route>
        <Route path="/profileCtxt" exact>
          <ProfileCtxt />
        </Route>
        <Route path="/contact" exact>
          <MyContact />
        </Route>
        <Route path="/experience" exact>
          <Exper />
        </Route>
        <Route path="/Mytodo" exact>
          <Mytodo />
        </Route>
        <Route path="/todoSimple" exact>
          <SimpleTodo />
        </Route>
        <Route path="/user" exact>
          <UserData />
        </Route>
        <Route path="/userProfile" exact>
          <DataFetching />
        </Route>
        <Route path="/post" exact>
          <PostData />
        </Route>
        <Route path="/*">
          <h1>Page Not Found</h1>
        </Route>
      </Switch>
    </Router>
  );
};

export default Routes;