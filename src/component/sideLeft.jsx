import React from "react"; //ini gak wajib, cuma klo pakai properti dari react, wajib pakai.
import "./sideLeft.css";
import {FiLinkedin, FiGithub, FiGitlab, FiInstagram, FiTwitter} from 'react-icons/fi';
// import {AiOutlineGitlab} from 'react-icons/ai';

function SideLeft() {
    return (
        <div className="left-container">
            <div className="icon">
            <a href="https://www.linkedin.com/in/aziza-az-zahrawani-1a524a1b9/">
            <FiLinkedin/></a>
            </div>

            <div className="icon">
            <a href="https://github.com/azizarwn">
            <FiGithub/></a>
            </div>

            <div className="icon">
            <a href="https://gitlab.com/azizarwn">
            <FiGitlab/></a>
            </div>

            <div className="icon">
            <a href="https://www.instagram.com/azizaazhrwn/">
            <FiInstagram/></a>
            </div>

            <div className="icon">
            <a href="https://gitlab.com/azizarwn">
            <FiTwitter/></a>
            </div>
        </div>
    );
}

export default SideLeft; 