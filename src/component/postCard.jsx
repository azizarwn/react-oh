import React from 'react';

function PostCard({...props}) {
    const { state } = props;

    return (
		<div className='user-data'>
            <h2>Passing Props</h2>
			{state.loading ? 'Loading' : state.post.map((user, index) => (
                <div className="user-item" key={index}>
                <p>{index + 1}. User ID: {user.userId}</p>
				<p>Title: {user.title}</p>
                <p>{user.body}</p>
                </div>
            ))}
			{state.error ? state.error : null}
		</div>
	)
   
}

export default PostCard;