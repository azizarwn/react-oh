import React from "react"; //ini gak wajib, cuma klo pakai properti dari react, wajib pakai.
import "./navbar.css";
import { Navbar, Nav } from 'react-bootstrap';

function Navbarx() {
    return (
        <div className="navbar-container">
            <Navbar  >
            <Nav className="mr-auto" >
            <Nav.Link style={{color: "#305F72"}} href="/">Home</Nav.Link>  
            <Nav.Link style={{color: "#305F72"}} href="/profileCtxt">About me</Nav.Link>
            <Nav.Link style={{color: "#305F72"}} href="/experience">Experience</Nav.Link>
            <Nav.Link style={{color: "#305F72"}} href="/contact">Contact</Nav.Link>
            <Nav.Link style={{color: "#305F72"}} href="/Mytodo">Todo-List</Nav.Link>
            <Nav.Link style={{color: "#305F72"}} href="/todoSimple">Simple-Todo-List</Nav.Link>
            <Nav.Link style={{color: "#305F72"}} href="/user">Users</Nav.Link>
            <Nav.Link style={{color: "#305F72"}} href="/userProfile">DataUsers</Nav.Link>
            <Nav.Link style={{color: "#305F72"}} href="/post">Posts-Data</Nav.Link>
            </Nav>
            </Navbar>
           
            
        </div>
    );
}

export default Navbarx; 