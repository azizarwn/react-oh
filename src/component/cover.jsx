import React from "react";
import "./cover.css";


function Cover() {
    return (
        <div className="cover">
            <div>
            <h1>Hi, my name is</h1>
            <h1>Aziza Az Zahrawani.</h1>
            <p>I am a recent graduate from State Polytechnic of Batam and 
            a Front End Developer Student in Glints Academy Batch 11. I've Learned 
            about HTML, CSS, Bootstrap, Javascript, React, and GIT. </p>
            </div> 
        </div>
    );
}

export default Cover; 
