import React from "react";
import { Button } from 'react-bootstrap';

function Todo({ todo, toggleComplete, removeTodo }) {
    function handleCheckboxClick() {
        toggleComplete(todo.id);
    }

    function handleRemoveClick() {
        removeTodo(todo.id);
    }

    return (
        <>
        <div style={{ 
                display: "flex", 
                alignItems: "center"
            }}
        >
        
        <input 
            type="checkbox" 
            onClick={handleCheckboxClick}
            style={{
                transform: "scale(2)"

            }}
        />
        <ul
            style={{
                color:"black",
                fontFamily: "Arial",
                fontSize: "1.5rem",
                fontWeight: "700",
                textDecoration: todo.completed ? "line-through" : null
            }}
        >
            {todo.task}
        </ul>
        <Button 
            style={{
                backgroundColor: "red",
                marginLeft: "4vh",
                transform: "scale(0.8)"
            }}
        onClick={handleRemoveClick}>X</Button>
        </div>
        </>
    );
}

export default Todo;    