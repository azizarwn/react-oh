import React, { useState } from "react";
import  { v4 as uuidv4 } from "uuid";
import { Form, Button } from 'react-bootstrap';

function TodoForm({ addTodo }) {
    const [todo, setTodo] = useState({
        id: "",
        task: "",
        completed: false
    });

    function handleTaskInputChange(item) {
        setTodo({ ...todo, task: item.target.value });
    }

    function handleSubmit(item) {
        item.preventDefault();
        if (todo.task.trim()) {
            addTodo({ ...todo, id: uuidv4() });
            
            //reset task input
            setTodo({ ...todo, task: "" });
        }
    }

    return (
        <form onSubmit={handleSubmit}>
            <Form.Control type="text"
                name="task"
                value={todo.task}
                onChange={handleTaskInputChange}
                placeholder="Enter task..."
            />
            <Button 
                type="submit" 
                variant="outline-light"
                style={{
                    backgroundColor: "#F18C8E"
                }}
            >Submit</Button>
        </form>
    );
}

export default TodoForm;