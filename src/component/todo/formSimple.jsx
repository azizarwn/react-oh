import React, { useState } from "react";
import { Form } from 'react-bootstrap';


const useInputValue = initialValue => {
    const [value, setValue] = useState(initialValue);

    return {
        value, 
        onChange: event => setValue(event.target.value),
        resetValue: () => setValue("")
    };
};

export const FormInput = ({ onSubmit }) => {
    const { resetValue, ...text } = useInputValue("");
  
    return (
      <Form
        onSubmit={e => {
          e.preventDefault();
          onSubmit(text.value);
          resetValue();
        }}>
        <Form.Control type="text"
            placeholder="Enter task..." 
            {...text} />
      </Form>
    );
  };
